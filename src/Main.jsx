import React from 'react';
import './common.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import webServicehelper from './common/WebServiceHelper'
import enums from './common/enums';
import Sectors from './Sectors'
import ReactDOM from 'react-dom';
import Reports from './Reports';

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
        }
    }

    componentDidMount() {
        this.renderSectors();
    }

    renderSectors() {
        ReactDOM.render(
            <Sectors
            />, document.getElementById('content')
        );
        this.setState({activeTab: "sectorsActive"})
    }

    renderDashboard() {
        ReactDOM.render(
            <Reports
            />, document.getElementById('content')
        );
        this.setState({activeTab: "dashboardActive"})
    }

    render() {
        return (
            <React.Fragment>
                <div className="d-flex flex-row">
                    <div className="col-1 p-0 sideBar">
                        <div className={`sideBar-item sidebar-item-empty d-flex justify-content-center align-items-center`}></div>
                        <div className={`sideBar-item d-flex justify-content-center align-items-center ${this.state.activeTab == "dashboardActive" ? "dashboardActive": ""}`} onClick={() => this.renderDashboard()}>Dashboard</div>
                        <div className={`sideBar-item d-flex justify-content-center align-items-center ${this.state.activeTab == "sectorsActive" ? "sectorsActive": ""}`} onClick={() => this.renderSectors()}>Sectors</div>
                    </div>
                    <div className="col-11 p-0">
                        <div className="topbar col-12 align-items-center"><h2>Construction Work Management System</h2></div>
                        <div id="content" className="col-12 m-0"></div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Main