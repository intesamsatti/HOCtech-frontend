import React from 'react';
import './common.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import webServicehelper from './common/WebServiceHelper'
import enums from './common/enums';

class SubPhases extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            project: props.project,
            addModalBool: false,
            phasesList: []
        }
    }

    componentDidMount() {
        this.getPhases()
    }

    getPhases() {
        webServicehelper.get(`/api/v1/phases?projectId=${this.props.project.id}`).then((res) => {
            if (res.data.statusCode == enums.responseCodes.SUCCESS) {
                this.setState({ phasesList: res.data.data })
            } else {
                alert(res.data.message)
            }
        }).catch((err) => {
            alert(err)
        })
    }

    handleNameChange(e){
        this.setState({name: e.target.value.trim()})
    }

    handleBudgetChange(e){
        this.setState({budget: e.target.value.trim()})
    }

    handleQuantityChange(e){
        this.setState({quantity: e.target.value.trim()})
    }

    addPhase(){
        if(!this.state.name){
            alert("Please enter phase name")
            return
        }
        if(!this.state.budget){
            alert("Please enter phase cost")
            return
        }

        if(!this.state.quantity){
            alert("Please enter quantity")
            return
        }

        let body = {
            name: this.state.name,
            budget: this.state.budget,
            quantity: this.state.quantity,
            projectId: this.props.project.id
        }
        webServicehelper.post(`/api/v1/phases`, body).then((res) => {
            if (res.data.statusCode == enums.responseCodes.SUCCESS) {
                this.setState({addModalBool: false})
                this.getPhases()
            } else {
                alert(res.data.message)
            }
        }).catch((err) => {
            alert(err)
        })
    }

    toggleAddModal(){
        this.setState({addModalBool: !this.state.addModalBool})
    }

    render() {
        return (
            <React.Fragment>
                <div className="d-flex col-9 m-auto p-4  justify-content-between">
                    <h2 className="m-0">{this.props.project.name} PHASES</h2>
                    <div className="d-flex flex-row">
                        <a className="align-self-center m-2 link" onClick={() => this.toggleAddModal()}>Add Phase</a>
                        <span className="addBtnGreen" onClick={() => this.toggleAddModal()}></span>
                    </div>
                </div>
                <div className="col-9 m-auto d-flex flex-row p-3 header">
                    <div className="col-3">Phase Name</div>
                    <div className="col-3">Budget (Rs)</div>
                    <div className="col-3">Quantity</div>
                    <div className="col-3">Physical Progress</div>
                </div>
                {!this.state.phasesList.length &&
                    <div className="col-9 m-auto d-flex flex-row p-3 listitem justify-content-center">
                        <div className="col-12 text-center"><h4>No Phases Added Yet.</h4></div>
                    </div>
                }
                <div className="ul col-9 m-auto p-0">
                    {this.state.phasesList.map((item) => {
                        return (
                            <div className="col-12 m-auto d-flex flex-row p-3 listitem">
                                <div className="col-3">{item.name}</div>
                                <div className="col-3">{item.cost}</div>
                                <div className="col-3">{item.quantity}</div>
                                <div className="col-3 text-success">IN PROGRESS</div>
                            </div>
                        )
                    })}
                </div>


                <Modal isOpen={this.state.addModalBool}>
                    {/* toggle={toggle} */}
                    <ModalHeader className="custom-modal-header">ADD PHASE</ModalHeader> {/*toggle={toggle*/}
                    <ModalBody className="info-modal-body m-auto">
                        <input className="form-control m-3" placeholder="Phase Name" onChange={(e) => this.handleNameChange(e)}></input>
                        <input type="number" className="form-control m-3" placeholder="Total Budget" onChange={(e) => this.handleBudgetChange(e)}></input>
                        <input className="form-control m-3" placeholder="Quantity" onChange={(e) => this.handleQuantityChange(e)}></input>
                        
                    </ModalBody>
                    <ModalFooter className="modal-footer-custom m-auto">
                        <Button className="confirmBtn" onClick={() => this.addPhase()}>ADD</Button>
                        <Button className="cancelBtn" onClick={() => this.toggleAddModal()}>CANCEL</Button>
                        {/* <Button color="secondary" disabled={isDisabled} onClick={() => cancelClickEve(false)}>{cancelBtnText}</Button> */}
                    </ModalFooter>
                </Modal>
            </React.Fragment>
        )
    }
}

export default SubPhases