import React from 'react';
import './common.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import webServicehelper from './common/WebServiceHelper'
import enums from './common/enums';
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import utility from './common/utility';
import ReactDOM from 'react-dom';
import Phases from './Phases'

class Projects extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            project: props.sector,
            open: false,
            projectsList: []
        }
    }

    componentDidMount() {
        this.getProjects()
    }

    renderPhases(project) {
        ReactDOM.render(
            <Phases
                project={project}
            />, document.getElementById('content')
        );
    }

    getProjects() {
        webServicehelper.get(`/api/v1/projects?sectorId=${this.props.sector.id}`).then((res) => {
            if (res.data.statusCode == enums.responseCodes.SUCCESS) {
                this.setState({ projectsList: res.data.data })
            } else {
                alert(res.data.message)
            }
        }).catch((err) => {
            alert(err)
        })
    }

    addPhases() {
        if (!this.state.name) {
            alert("please enter name")
            return
        }
        if (!this.state.budget) {
            alert("please enter budget")
            return
        }

        if (!this.state.date) {
            alert("please enter date")
            return
        } let body = {
            name: this.state.name,
            adpCost: this.state.budget,
            adminApprovalDate: this.state.date/1000,
            sectorId: this.props.sector.id,
            status: enums.phaseStatuses.PENDING,
            approvingAuthority: this.state.approvingAuthority
        }
        webServicehelper.post('/api/v1/projects', body).then((res) => {
            this.setState({
                name: "",
                budget: "",
                adpAllocation: "",
                open: false
            })
            this.getProjects()
        }).catch((err) => {
            alert(err)
        })

    }

    handleDateChange(e) {
        this.setState({ date: e.getTime() })
    }

    handleNameChange(e) {
        this.setState({ name: e.target.value })
    }

    handleBudgetChange(e) {
        this.setState({ budget: e.target.value })
    }
    handleApprovingAuthorityChange(e) {
        this.setState({ approvingAuthority: e.target.value })
    }

    toggleModal() {
        this.setState({ open: !this.state.open })
    }

    render() {
        return (
            <React.Fragment>
                <div className="d-flex col-9 m-auto p-4  justify-content-between">
                    <h2 className="m-0">{this.props.sector.name} PROJECTS</h2>
                    <div className="d-flex flex-row">
                        <a className="align-self-center m-2 link" onClick={() => this.toggleModal()}>Add Project</a>
                        <span className="addBtnGreen" onClick={() => this.toggleModal()}></span>
                    </div>
                </div>
                <div className="col-9 m-auto d-flex flex-row p-3 header">
                    <div className="col-3">Project Name</div>
                    <div className="col-3">Approval Date</div>
                    <div className="col-3">ADP Cost (Rs)</div>
                    <div className="col-3">Approving Authority</div>
                </div>
                {!this.state.projectsList.length &&
                    <div className="col-9 m-auto d-flex flex-row p-3 listitem justify-content-center">
                        <div className="col-12 text-center"><h4>No Projects Added Yet.</h4></div>
                    </div>
                }
                <div className="ul col-9 m-auto p-0">
                    {this.state.projectsList.map((item) => {
                        return (
                            <div className="col-12 m-auto d-flex flex-row p-3 listitem" onClick={() => this.renderPhases(item)}>
                                <div className="col-3">{item.name}</div>
                                <div className="col-3">{utility.getFormattedDate(item.adminApprovalDate)}</div>
                                <div className="col-3">{item.adpCost}</div>
                                <div className="col-3">{item.approvingAuthority}</div>
                            </div>
                        )
                    })}
                </div>

                <Modal isOpen={this.state.open}>
                    <ModalHeader className="custom-modal-header">ADD PROJECT</ModalHeader> {/*toggle={toggle*/}
                    <ModalBody className="info-modal-body m-auto">
                        <input className="form-control m-3" placeholder="Project Name" onChange={(e) => this.handleNameChange(e)}></input>
                        <input type="number" className="form-control m-3" placeholder="ADP Cost" onChange={(e) => this.handleBudgetChange(e)}></input>
                        <DatePicker
                            className="datePicker form-control mx-3"
                            selected={this.state.date}
                            onChange={(e) => this.handleDateChange(e)}
                            placeholderText="Approval Date"
                        />
                        <input type="text" className="form-control m-3" placeholder="Approving Authority" onChange={(e) => this.handleApprovingAuthorityChange(e)}></input>
                    </ModalBody>
                    <ModalFooter className="modal-footer-custom m-auto">
                        <Button className="confirmBtn" onClick={() => this.addPhases()}>ADD</Button>
                        <Button className="cancelBtn" onClick={() => this.toggleModal()}>CANCEL</Button>
                    </ModalFooter>
                </Modal>
            </React.Fragment>
        )
    }
}

export default Projects