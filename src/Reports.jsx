import React from 'react';
import './common.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import webServicehelper from './common/WebServiceHelper'
import enums from './common/enums';
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import utility from './common/utility';
import ReactDOM from 'react-dom';
import Phases from './Phases'

class Reports extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            startDate: "",
            endDate: "",
            summaryList: []
        }
    }

    componentDidMount() {
        //this.fetchSummary()
    }


    handleStartDateChange(e) {
        this.setState({ startDate: e.getTime() })
    }

    handleEndDateChange(e) {
        this.setState({ endDate: e.getTime() })
    }

    toggleModal() {
        this.setState({ open: !this.state.open })
    }

    fetchSummary() {
        // if (!this.state.startDate) {
        //     alert("Please select a start date")
        // }

        // if (!this.state.endDate) {
        //     alert("Please select a end date")
        // }
        webServicehelper.get(`/api/v1/reports?startDate=${this.state.startDate/1000}&endDate=${this.state.endDate/1000}`).then((res) => {
            if (res.data.statusCode == enums.responseCodes.SUCCESS) {
                this.setState({ projectsList: res.data.data })
            } else {
                alert(res.data.message)
            }
        }).catch((err) => {
            alert(err)
        })

    }

    render() {
        return (
            <React.Fragment>
                <div className="d-flex col-9 m-auto p-4  justify-content-between">
                    <h2 className="m-0">SUMMARY</h2>
                </div>
                <div className="d-flex flex-row col-9 m-auto p-4  justify-content-start">
                    <DatePicker
                        className="datePicker form-control mx-2"
                        selected={this.state.startDate}
                        onChange={(e) => this.handleStartDateChange(e)}
                        placeholderText="Start Date"
                    />
                    <DatePicker
                        className="datePicker form-control mx-3"
                        selected={this.state.endDate}
                        onChange={(e) => this.handleEndDateChange(e)}
                        placeholderText="End Date"
                    />
                    <Button className="fetchSummary" onClick={() => this.fetchSummary()}>Fetch</Button>
                </div>
                <div className="col-9 m-auto d-flex flex-row p-3 header">
                    <div className="col-3">Sector Name</div>
                    <div className="col-3">Project Name</div>
                    <div className="col-3">Allocation</div>
                    <div className="col-3">Released</div>
                </div>
                {!this.state.summaryList.length &&
                    <div className="col-9 m-auto d-flex flex-row p-3 listitem justify-content-center">
                        <div className="col-12 text-center"><h4>Select start and end date to fetch summary</h4></div>
                    </div>
                }
                <div className="ul col-9 m-auto p-0">
                    {this.state.summaryList.map((item) => {
                        return (
                            <div className="col-12 m-auto d-flex flex-row p-3 listitem" onClick={() => this.renderPhases(item)}>
                                <div className="col-3">{item.name}</div>
                                <div className="col-3">{utility.getFormattedDate(item.adminApprovalDate)}</div>
                                <div className="col-3">{item.adpCost}</div>
                                <div className="col-3">{item.approvingAuthority}</div>
                            </div>
                        )
                    })}
                </div>

                <Modal isOpen={this.state.open}>
                    <ModalHeader className="custom-modal-header">ADD PROJECT</ModalHeader> {/*toggle={toggle*/}
                    <ModalBody className="info-modal-body m-auto">
                        <input className="form-control m-3" placeholder="Project Name" onChange={(e) => this.handleNameChange(e)}></input>
                        <input type="number" className="form-control m-3" placeholder="ADP Cost" onChange={(e) => this.handleBudgetChange(e)}></input>
                        <DatePicker
                            className="datePicker form-control mx-3"
                            selected={this.state.date}
                            onChange={(e) => this.handleDateChange(e)}
                            placeholderText="Approval Date"
                        />
                        <input type="text" className="form-control m-3" placeholder="Approving Authority" onChange={(e) => this.handleApprovingAuthorityChange(e)}></input>
                    </ModalBody>
                    <ModalFooter className="modal-footer-custom m-auto">
                        <Button className="confirmBtn" onClick={() => this.addPhases()}>ADD</Button>
                        <Button className="cancelBtn" onClick={() => this.toggleModal()}>CANCEL</Button>
                    </ModalFooter>
                </Modal>
            </React.Fragment>
        )
    }
}

export default Reports