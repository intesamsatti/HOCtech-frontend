import React from 'react';
import './common.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import webServicehelper from './common/WebServiceHelper'
import enums from './common/enums';
import Projects from './Projects'
import ReactDOM from 'react-dom';

class Sectors extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            sectorsList: []
        }
    }

    componentDidMount() {
        this.getSectors()
    }
    handleNameChange(e) {
        this.setState({ name: e.target.value })
    }

    handleBudgetChange(e) {
        this.setState({ budget: e.target.value })
    }


    getSectors() {
        webServicehelper.get('/api/v1/sectors').then((res) => {
            if (res.data.statusCode == enums.responseCodes.SUCCESS) {
                this.setState({ sectorsList: res.data.data })
            } else {
                alert(res.data.message)
            }
        }).catch((err) => {
            alert(err)
        })
    }

    addSector() {
        if (!this.state.name) {
            alert("please enter name")
            return
        }
        webServicehelper.post('/api/v1/sectors', { sectorName: this.state.name }).then((res) => {
            this.setState({
                name: "",
                open: false
            })
            this.getSectors()
        }).catch((err) => {
            alert(err)
        })
    }

    toggleAddModal() {
        this.setState({ open: !this.state.open })
    }

    renderPhases(sector) {
        ReactDOM.render(
            <Projects
                sector={sector}
            />, document.getElementById('content')
        );
    }

    render() {
        return (
            <React.Fragment>
                <div className="d-flex col-9 m-auto p-4  justify-content-between">
                    <h2 className="m-0">SECTORS</h2>
                    <div className="d-flex flex-row">
                        <a className="align-self-center m-2 link" onClick={() => this.toggleAddModal()}>Add Sectors</a>
                        <span className="addBtnGreen" onClick={() => this.toggleAddModal()}></span>
                    </div>
                </div>
                <div className="col-9 m-auto d-flex flex-row p-3 header">
                    <div className="col-12">NAME</div>

                </div>
                {!this.state.sectorsList.length &&
                    <div className="col-9 m-auto d-flex flex-row p-3 listitem justify-content-center">
                        <div className="col-12 text-center"><h4>No Sectors Added Yet.</h4></div>
                    </div>
                }
                <div className="ul col-9 m-auto p-0">
                    {this.state.sectorsList.map((item) => {
                        return (
                            <div className="col-12 m-auto d-flex flex-row p-3 listitem" onClick={() => this.renderPhases(item)}>
                                <div className="col-12">{item.name}</div>
                            </div>
                        )
                    })}
                </div>

                <Modal isOpen={this.state.open}>
                    <ModalHeader className="custom-modal-header">ADD SECTOR</ModalHeader> {/*toggle={toggle*/}
                    <ModalBody className="info-modal-body m-auto">
                        <input className="form-control m-3" onChange={(e) => this.handleNameChange(e)} placeholder="Sector Name" ></input>
                    </ModalBody>
                    <ModalFooter className="modal-footer-custom m-auto">
                        <Button className="confirmBtn" onClick={() => this.addSector()}>ADD</Button>
                        <Button className="cancelBtn" onClick={() => this.toggleAddModal()}>CANCEL</Button>
                    </ModalFooter>
                </Modal>
            </React.Fragment>
        )
    }
}

export default Sectors