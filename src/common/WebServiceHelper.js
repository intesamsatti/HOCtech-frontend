import axios from 'axios';
import config from './config';
import enums from './enums';

function getCookie(name){
   const value = `; ${document.cookie}`;
   const parts = value.split(`; ${name}=`);
   if (parts.length === 2) return parts.pop().split(';').shift();
}

let webServiceHelper = {
    baseURL: config.baseURL,
    token: null,
    get: (url) => {
        if(!webServiceHelper.token){
            try {
                let token = JSON.parse(decodeURIComponent(getCookie('licenseUserDetails'))).token || null;
                webServiceHelper.token = token;
            } catch (error) {}
        }
        url = `${webServiceHelper.baseURL}${url}`
        return new Promise((resolve, reject)=>{
            if(!url) {
                reject();
                return;
            }
            axios.get(url, {
                headers: {
                    token: webServiceHelper.token
                }
            })
             .then((response)=>{
                console.log(response);
                let statusCode = response.data.statusCode;
                if(statusCode && (statusCode === enums.responseCodes.INVALID_TOKEN || statusCode === enums.responseCodes.TOKEN_EXPIRED)){
                    // document.cookie.split(";").forEach(function(c) { 
                    //     document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); 
                    // });
                    // window.location.reload();
                    //return;
                }
                resolve(response);
             })
             .catch((err)=>{
                console.log(err);
                reject(err);
             });
        });
    },

    post: (url, body) => {
        return new Promise((resolve, reject) => {
            if(!url) {
                reject();
                return;
            }
            url = `${webServiceHelper.baseURL}${url}`;
            axios.post(url, body, { headers: { token: webServiceHelper.token }})
            .then((response)=>{
               console.log(response);
               resolve(response);
            })
            .catch((err)=>{
               console.log(err);
               reject(err);
            });
        });
    },

    put: (url, body) => {

        if(!webServiceHelper.token){
            try {
                let token = JSON.parse(decodeURIComponent(getCookie('licenseUserDetails'))).token;
                webServiceHelper.token = token;
            } catch (error) {}
        }
        
        return new Promise((resolve, reject) => {
            if(!url) {
                reject();
                return;
            }
            url = `${webServiceHelper.baseURL}${url}`
            axios.put(url, body, { headers: { token: webServiceHelper.token }})
            .then((response)=>{
               console.log(response);
               resolve(response);
            })
            .catch((err)=>{
               console.log(err);
               reject(err);
            });
        });
    },

    initBasics: (data) => {
        webServiceHelper.token = data.token;
    }
}

export default webServiceHelper;