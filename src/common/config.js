let version = `Ver.1.0.0`;
let development = {
    "baseURL": "https://127.0.0.1:5000",
    version,
}

let staging = {
    "baseURL": "https://127.0.0.1:5000",
    version,
}


let production = {
    "baseURL": 
        window.location.port ? 
            "https://" + window.location.hostname + ":" + window.location.port: "https://" + window.location.hostname + "" ,
            version,
}


let config = {};
// console.log(process.env.REACT_APP_ENVIRONMENT)
// if(process.env.REACT_APP_ENVIRONMENT === 'production') {
//     config = production;
// } else if(process.env.REACT_APP_ENVIRONMENT === 'staging') {
//     config = staging;
// } else {}
config = development;


export default config