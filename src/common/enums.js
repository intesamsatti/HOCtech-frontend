const enums = {};

enums.COOKIE_EXPIRY_TIME = 15778476;

enums.status = {
    "ACTIVE": 1,
    "PENDING": 2,
    "ACCEPTED": 3,
    "REJECTED": 4,
    "SENT": 5,
    "RECEIVED": 6,
    "DELETED": 7,
    "BLOCKED": 8,
    "INACTIVE": 9,
    "EXPIRED": 10
};

enums.responseCodes = {
    "SUCCESS": 0,
    "INVALID_PARAMS": -1,
    "CONNECTION_POOL_ERROR": -2,
    "SQL_QUERY_ERROR": -3,
    "EMAIL_SENDING_FAIL": -4,
    "PERMISSION_DENIED": -5,
    "INVALID_TOKEN": -7,
    "TOKEN_EXPIRED": -8,
};

enums.responseMessages = {
    "INVALID_PARAMS" : "Invalid parameters",
    "CONNECTION_POOL_ERROR": "Something went wrong",
    "SQL_QUERY_ERROR": "Something went wrong",
    "INVALID_USER_NAME": "Invalid username",
    "INVALID_TOKEN": "Invalid token",
    "TOKEN_EXPIRED": "Token expired"
};

enums.phaseStatuses = {
    "PENDING": 1,
    "INPROGRESS": 2,
    "PAID": 3,
    "COMPLETED": 4
};

export default enums;