'use strict';
import enums from './enums';
import moment from 'moment';

class utility {
    static getFormattedDate(timeStamp){
        if(!timeStamp) return;
        let date = new Date(timeStamp * 1000);
        return date.getDate() + ' ' + date.toLocaleString('default', {month: 'short'}) + ' ' + date.getFullYear();
    }

    // static getFormattedDagWithDate(timeStamp){
    //     if(!timeStamp) return;
    //     let date = new Date(timeStamp * 1000);
    //     let dateForDay = date;
    //     console.log(date);
    //     console.log(date.toLocaleString('default', {month: 'short'}));
    //     date =  date.getDate() + ' ' + date.toLocaleString('default', {month: 'short'}) + ' ' + date.getFullYear();
    //     console.log(date);
    //     let day = moment(dateForDay).format('ddd');
    //     console.log(day);
    //     return `${day}, ${date}`
    // }

    // static getFormattedDateTime(timeStamp){
    //     if(!timeStamp) return;
    //     let date = new Date(timeStamp * 1000);
    //     let formattedDate = date.getDate() + ' ' + date.toLocaleString('default', {month: 'short'}) + ' ' + date.getFullYear();
    //     let formattedTime = moment(date).format('hh:mm A');
    //     return formattedDate + ' ' + formattedTime;
    // }

    // static getFormattedTime(timeStamp){
    //     if(!timeStamp) return;
    //     let date = new Date(timeStamp * 1000);
    //     //let formattedDate = date.getDate() + ' ' + date.toLocaleString('default', {month: 'short'}) + ' ' + date.getFullYear();
    //     let formattedTime = moment(date).format('hh:mm A');
    //     return formattedTime;
    // }

    // static getFormattedDuration(startDateTime, endDateTime){
    //     let startTime = moment(startDateTime * 1000);
    //     let endTime = moment(endDateTime * 1000);
    //     // let diff =  endTime.diff(startTime, 'seconds');
    //     try {
    //         let duration = moment.duration(endTime.diff(startTime));
    //         let days = duration._data.days,
    //             hours = duration._data.hours,
    //             minutes = duration._data.minutes,
    //             months = duration._data.months,
    //             years = duration._data.years;
    //         let diff = ``;
    //         if(years) {
    //             diff = `${years} ${(years > 1) ?  t("Years") :  t("Year")}`;
    //         }
    //         if(months) {
    //             diff += ` ${months} ${(months > 1) ?  t("Months") :  t("Month")}`;
    //         }
    //         if(days) {
    //             diff += ` ${days} ${(days > 1) ?   t("Days") :  t("Day")}`;
    //         }
    //         if(hours) {
    //             diff += ` ${hours} ${(hours > 1) ?  t("Hours") :  t("Hour")}`;
    //         }
    //         if(minutes) {
    //             diff += ` ${minutes} ${(minutes > 1) ?  t("Minutes") :  t("Minute")}`;
    //         }

    //         return diff;
    //     } catch (error) {
    //         console.log("error");
    //     }
    // }

    // static getTimeStamp(){
    //     return Math.floor(Date.now()/1000);
    // }

    // static getCompanyStatusText(type){
    //     if(type == null || type == undefined){
    //         return '---';
    //     }
    //     if(!Object.values(enums.status).includes(type)){
    //         return '';
    //     }
    //     let statusText = Object.keys(enums.status)[Object.values(enums.status).indexOf(type)].toLowerCase();
    //     if (statusText == 'sent' || statusText == 'received'){
    //         statusText = 'pending';
    //     }
    //     return statusText;
    // }

    // static getCompanyPackageText(type){
    //     if(type == null || type == undefined){
    //         return '---';
    //     }
    //     if(!Object.values(enums.paymentPlan).includes(type)){
    //         return '';
    //     }
    //     return t(enums.paymentPlanStrings[type]);
    //     let statusText = Object.keys(enums.paymentPlan)[Object.values(enums.paymentPlan).indexOf(type)].toLowerCase().replace("_", " ");
    //     statusText = t(statusText)
    //     return statusText;
    // }

    // static hasWhiteSpace(text){
    //     if(!text){
    //         return false;
    //     }
    //     return text.indexOf(' ') > -1;
    // }

    // static getTimeDiffForNotification(timestamp) {
    //     var diff = ((Date.now() / 1000) - timestamp) / 60;
    //     if (diff < 60) {
    //         return (
    //             Math.floor(diff) <= 0 ? {time: '', unit: t("JustNow")} : {time: Math.floor(diff), unit: t("MinAgo")}
    //         )
    //     }
    //     else {
    //         diff = diff / 60;
    //         if (diff < 24) {
    //             return (
    //                 {
    //                     time: Math.floor(diff),
    //                     unit: t("HoursAgo")
    //                 }
    //             )
    //         } else {
    //             diff = diff / 24;
    //             if (diff < 7) {
    //                 return (
    //                     {
    //                         time: Math.floor(diff),
    //                         unit: t("DaysAgo")
    //                     }
    //                 )
    //             } else {
    //                 diff = diff / 7;
    //                 return (
    //                     {
    //                         time: Math.floor(diff),
    //                         unit: t("WeeksAgo")
    //                     }
    //                 )
    //             }

    //         }
    //     }
    // }

    // static getCalendarDateTime(secTimeStamp){
    //     if(!secTimeStamp){
    //         return null;
    //     } // get formatted date/time for datetime-local input
    //     let formatted = ((new Date((secTimeStamp * 1000) - (new Date(secTimeStamp * 1000).getTimezoneOffset()*60000))).toISOString().slice(0, -8));
    //     return formatted;
    // }

    // static getCalendarDateTimeIos(secTimeStamp){
    //     if(!secTimeStamp){
    //         return null;
    //     } // get formatted date/time for datetime-local input
    //     let formatted = ((new Date((secTimeStamp * 1000) - (new Date(secTimeStamp * 1000).getTimezoneOffset()*60000))).toISOString().slice(0, -8));
    //     let formted = moment(formatted).format('MM/DD/YYYY hh:mm A')
    //     return formted;
    // }

    // static isANumber(numberToCheck){
    //     if(numberToCheck != null && numberToCheck != undefined){
    //         if(typeof(numberToCheck) != 'string' ){
    //             numberToCheck = numberToCheck.toString();
    //         }
    //         numberToCheck = numberToCheck.replace(/\-/g, '');
    //         return  /^[+]\d+$/.test(numberToCheck);
    //     }
    // }
    // static getDateForPicker(){
    //     var today = new Date();
    //     var dd = today.getDate();
    //     var mm = today.getMonth()+1; //January is 0!
    //     var yyyy = today.getFullYear();
    //     if(dd<10){
    //             dd='0'+dd
    //         } 
    //         if(mm<10){
    //             mm='0'+mm
    //         } 

    //     today = yyyy+'-'+mm+'-'+dd;
    //     return today;
    // }

    // static getBrowserLanguage(){
    //     var lang = navigator.language || navigator.userLanguage;
    //     var langPrefix = lang.split('-')
    //     var language = "lang_"+ langPrefix[0];
    //     if(!enums.supportedLanguages[language]){
    //         return "lang_en"
    //     }else{
    //         return language;
    //     }
    // }
    
    // static datesAreOnSameDay(time1, time2){
    //     let date1 = new Date(time1 * 1000);
    //     let date2 = new Date(time2 * 1000);
    //     return date1.getFullYear() === date2.getFullYear() &&
    //     date1.getMonth() === date2.getMonth() &&
    //     date1.getDate() === date2.getDate();
    // }

    // static alertify(type, message,title){

    //     if(!type || !message ) return;
    //       switch (type) {
    //         case 'info':
    //           NotificationManager.info(title, message, 3000);
    //           break;
    //         case 'success':
    //           NotificationManager.success(title, message, 3000);
    //           break;
    //         case 'warning':
    //           NotificationManager.warning(title, message, 3000);
    //           break;
    //         case 'error':
    //           NotificationManager.error(title, message, 3000);
    //           break;
    //       }
    // }

    // static checkBrowser () {
    //     let sBrowser; 
    //     let sUsrAg = navigator.userAgent
    //     let isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
    //     sUsrAg &&
    //     sUsrAg.indexOf('CriOS') == -1 &&
    //     sUsrAg.indexOf('FxiOS') == -1;
    //     if (sUsrAg.indexOf('Firefox') > -1 || typeof InstallTrigger !== 'undefined') {
    //       sBrowser = 'firefox'
    //       // "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0"
    //     } else if (sUsrAg.indexOf('SamsungBrowser') > -1) {
    //       sBrowser = 'samsunginternet'
    //       // "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G955F Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36
    //     } else if (sUsrAg.indexOf('Opera') > -1 || sUsrAg.indexOf('OPR') > -1) {
    //       sBrowser = 'opera'
    //       // "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106"
    //     } else if (sUsrAg.indexOf('Trident') > -1 || !!document.documentMode) {
    //       sBrowser = 'ie'
    //       // "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; Zoom 3.6.0; wbx 1.0.0; rv:11.0) like Gecko"
    //     } else if (sUsrAg.indexOf('Edge') > -1) {
    //       sBrowser = 'edge'
    //       // "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299"
    //     } else if (sUsrAg.indexOf('plugin') > -1) {
    //       sBrowser = 'chrome'
    //     } else if (sUsrAg.indexOf('Chrome') > -1) {
    //       sBrowser = 'chrome'
    //       // "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36"
    //     } else if (isSafari) {
    //       sBrowser = 'safari'
    //       // "Mozilla/5.0 (iPhone; CPU iPhone OS 11_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1 980x1306"
    //     } else {
    //       sBrowser = 'unknown'
    //     }
    //     console.log("**************"+ sBrowser + "*********************");
    //     return sBrowser
    // }

    // static goBack(){
    //     (window.history.state && !window.history.state.key) ? history.push('/'): history.back();
    // }

    // static getGenderText(genderCode) {
    //     let gender = '---'
    //     if(!genderCode) {
    //         return gender;
    //     }
    //     Object.keys(enums.gender)
    //     .forEach(function eachKey(key) { 
    //         if(enums.gender[key] == genderCode) {
    //             gender = key
    //         }
    //     });
    //     return gender;
    // }

    // static getFormatedDOB(secTimeStamp){
    //     if(!secTimeStamp){
    //         return null;
    //     } // get formatted date/time for datetime-local input
    //     let formatted = ((new Date((secTimeStamp * 1000) - (new Date(secTimeStamp * 1000).getTimezoneOffset()*60000))).toISOString().slice(0, -8));
    //     return formatted.split('T')[0];
    // }
}

export default utility;