import React from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import Projects from './Projects'
import Phases from './Phases'
import Sectors from './Sectors'
import Main from './Main'

const Routes = () => {
    return (
        <Router>
            <Switch>
                <Route path='/' exact component={ Main }/>
                <Redirect to="/"/> {/* Redirect must be on end of all routes */}
            </Switch>
        </Router>
    );
}
export default Routes;